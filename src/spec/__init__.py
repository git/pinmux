from spec import m_class
from spec import c_class
from spec import i_class
from spec import minitest
from spec import microtest
from spec import ls180
from spec import ngi_router
from spec.gen import specgen
from spec.testing import dummytest

modules = {'m_class': m_class,
           'c_class': c_class,
           'i_class': i_class,
           'minitest': minitest,
           'microtest': microtest,
           'ls180': ls180,
           'ngi_router': ngi_router,
           }
